/**
 * Plugin wrapper for registering routes.
 *
 * @function router
 * @param   {Object[]} routes Routes to register
 * @returns {Function}
 */
module.exports = function router (routes) {
  /**
   * Route registering plugin for our FastifyJS application.
   *
   * @function plugin
   * @param  {Object}   fastify Fastify instance
   * @param  {Object}   options Registration options
   * @param  {Function} done    Complete plugin registration
   */
  return function plugin (fastify, options, done) {
    // Register all of our routes
    routes.forEach((route) => fastify.route(route))

    // Let Fastify know we're done registering our module
    done()
  }
}
