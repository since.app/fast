// Packages imports
const fastify = require('fastify')
const cors = require('fastify-cors')

/**
 * Starts a FastifyJS server.
 *
 * @async
 * @function server
 * @param {Function[]} plugins FastifyJS plugins to register
 * @param {Object}     options Server options
 */
module.exports = async function server (plugins, options) {
  /**
   * Initialize our Fastify application.
   *
   * @type {Object}
   */
  const app = fastify({ logger: true })

  // Register core plugins
  app.register(cors, options.cors || {
    origin: true,
    credentials: true
  })

  try {
    // Register any plugins passed
    plugins.forEach((plugin) => {
      app.register(plugin, options)
    })

    // Start actual server
    await app.listen(options.port)
  } catch (e) {
    // Print error to console
    app.log.error(e)

    // Stop app entirely
    process.exit(1)
  }
}
