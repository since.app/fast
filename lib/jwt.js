// Package imports
const jsonwebtoken = require('fastify-jwt')
const make = require('fastify-plugin')

/**
 * Plugin wrapper to register fastify-jwt.
 *
 * @function jwt
 * @returns {Function} FastifyJS plugin registering function
 */
module.exports = function jwt () {
  /**
   * Plugin to register the fastify-jwt plugin for all requests.
   *
   * @function plugin
   * @param {Object}   fastify Fastify instance
   * @param {Object}   options Registration options
   * @param {Function} done    Complete plugin registration
   */
  function plugin (fastify, options, done) {
    // Register the JWT plugin
    fastify.register(jsonwebtoken, { secret: options.token.secret })

    // Add hook to validate all requests
    fastify.addHook('onRequest', async (request, reply) => {
      try {
        // Try verifying the JWT which will populate a fastify.user object
        await request.jwtVerify()
      } catch (err) {
        // Sending an error before the router can handle the request prevents it from doing so
        reply.send(err)
      }
    })

    // Let Fastify know we're done
    done()
  }

  // Using fastify-plugin to get JWT authentication on the entire app
  return make(plugin)
}
