// Module imports
const jwt = require('./lib/jwt')
const router = require('./lib/router')
const server = require('./lib/server')

// Module exports the FastifyJS server creator
module.exports = server

// Module also exports a FastifyJS JSON Web Token authentication plugin
module.exports.jwt = jwt

// Module also exports a FastifyJS route registering plugin
module.exports.router = router
